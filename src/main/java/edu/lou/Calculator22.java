package edu.lou;

public class Calculator22 {

	public Integer doBinaryOp(Integer f1, Integer f2, FunctionalBinaryOperation<Integer> op) {
		return op.apply(f1, f2);
	}

	@FunctionalInterface
	public static interface FunctionalBinaryOperation<T> {
		T apply(T t1, T t2);
	}
}