package edu.lou;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.ForkJoinPool;

import org.junit.Test;


public class PrimeCounterTest {

    @Test
    public void range_is_ok_then_count_is_correct() {
        ExecutorService es = new ForkJoinPool(4);
        PrimeCounter pc = new PrimeCounter(es);
        assertEquals(4, (int) pc.apply(0, 10));
        es.shutdown();
    }
}