package edu.lou;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class Calculator11Test {
	
	private Calculator11 subject;
	
	@Before
	public void setUp() {
		subject = new Calculator11();
	}

	@Test
	public void addSquare() {
		// given
		Integer three = 3;
		Integer two = 2;
		
		// when
		Integer res = subject.addSquare(three, two);
		
		// then
		assertEquals(13, (int) res);
	}
}
