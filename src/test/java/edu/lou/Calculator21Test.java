package edu.lou;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import edu.lou.Calculator21.BinaryOperation;

public class Calculator21Test {

	private Calculator21 subject;

	@Before
	public void setUp() {
		subject = new Calculator21();
	}

	@Test
	public void addSquare() {
		// given
		Integer three = 3;
		Integer two = 2;

		// when
		Integer res = subject.doBinaryOp(three, two, new BinaryOperation<Integer>() {
			// https://www.facebook.com/lou.andria261/videos/1384847894951942
			@Override
			public Integer apply(Integer n1, Integer n2) {
				return n1 * n1 + n2 * n2;
			}
		});

		// then
		assertEquals(13, (int) res);
	}
	
	@Test
	public void multSquare() {
		// given
		Integer three = 3;
		Integer two = 2;

		// when
		Integer res = subject.doBinaryOp(three, two, new BinaryOperation<Integer>() {
			@Override
			public Integer apply(Integer n1, Integer n2) {
				return (n1 * n1) * (n2 * n2);
			}
		});

		// then
		assertEquals(36, (int) res);
	}
}
