package edu.lou;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class FractionTest {
	
	@Test
	public void lombok() {
		Fraction f1 = new Fraction(1, 2);
		Fraction f2 = new Fraction(1, 2);
		assertEquals(f1, f2); // lombok equality is semantic, not physical
	}
}
